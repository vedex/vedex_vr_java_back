package com.vedex.backend.common.services;

import com.vedex.backend.common.dbmodel.database.DBHelper;
import com.vedex.backend.common.dto.*;
import com.vedex.backend.common.utils.ModelConverter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupsService {
    public GroupsService() {}

    public void createGroup(int userId, Group group) {
        DBHelper.createGroup(userId, ModelConverter.convertGroup(group));
    }

    public List<Invitation> getInvitations(int userId) {
        return DBHelper.getInvitations(userId).stream().map(ModelConverter::convertInvitation)
                .collect(Collectors.toList());
    }

    public List<GroupMember> getMembers(int userId, int groupId) {
        return DBHelper.getGroupMembers(userId, groupId).stream()
                .map(ModelConverter::convertGroupMember).collect(Collectors.toList());
    }

    public List<GroupMembership> getGroups(int userId) {
        return DBHelper.getGroups(userId).stream().map(ModelConverter::convertGroupMembership)
                .collect(Collectors.toList());
    }

    public void createInvitation(int userId, Invitation invitation) {
        DBHelper.createInvitation(userId, ModelConverter.convertInvitation(invitation));
    }

    public void respondInvitation(int userId, InvitationResponse response) {
        DBHelper.respondInvitation(userId, response.getInvitationId(), response.isAccepted());
    }

    public void editGroup(int userId, Group newState) {
        DBHelper.editGroup(userId, ModelConverter.convertGroup(newState));
    }

    public void editMemberAccess(int editorId, GroupMembership newRoleDescription) {
        DBHelper.editMemberAccess(editorId,
                ModelConverter.convertGroupMembership(newRoleDescription));
    }

    public void deleteGroup(int userId, int groupId) {
        DBHelper.deleteGroup(userId, groupId);
    }
}
