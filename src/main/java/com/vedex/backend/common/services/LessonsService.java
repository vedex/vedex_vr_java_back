package com.vedex.backend.common.services;

import com.vedex.backend.common.dbmodel.database.DBHelper;
import com.vedex.backend.common.dto.Lesson;
import com.vedex.backend.common.utils.ModelConverter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LessonsService {
    public LessonsService() {}

    public List<String> getSubjects() {
        return DBHelper.getSubjects();
    }

    public List<Lesson> getLessons(String subject) {
        return DBHelper.getLessons(subject).stream().map(ModelConverter::convertLesson)
                .collect(Collectors.toList());
    }
}
