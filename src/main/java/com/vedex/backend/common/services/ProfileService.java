package com.vedex.backend.common.services;

import com.vedex.backend.common.dbmodel.database.DBHelper;
import com.vedex.backend.common.dto.Profile;
import com.vedex.backend.common.utils.ModelConverter;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfileService {

    public ProfileService() {}

    public Profile getProfile(int userId) {
        return ModelConverter.convertProfile(DBHelper.getProfile(userId));
    }

    public void updateProfile(int userId, Profile profile) {
        DBHelper.updateProfile(userId, ModelConverter.convertProfile(profile));
    }

    public void updatePhoto(int userId, String photoUrl) {
        DBHelper.updatePhoto(userId, photoUrl);
    }

    public List<Profile> findProfiles(Profile matchProfileData) {
        return DBHelper.findProfiles(ModelConverter.convertProfile(matchProfileData)).stream()
                .map(ModelConverter::convertProfile).collect(Collectors.toList());
    }

    public Profile getProfile(String login) {
        return ModelConverter.convertProfile(DBHelper.getProfile(login));
    }

    public Profile login(String username, String password) {
        return ModelConverter.convertProfile(DBHelper.getProfile(username, password));
    }

    public void createProfile(Profile profile) throws SQLException {
        DBHelper.insertProfile(ModelConverter.convertProfile(profile));
    }
}
