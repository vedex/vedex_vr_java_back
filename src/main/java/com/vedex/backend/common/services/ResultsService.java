package com.vedex.backend.common.services;

import com.vedex.backend.common.dbmodel.database.DBHelper;
import com.vedex.backend.common.dto.Result;
import com.vedex.backend.common.utils.ModelConverter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ResultsService {
    public ResultsService() {}

    public void addResult(int userId, Result result) {
        DBHelper.addResult(userId, ModelConverter.convertResult(result));
    }

    public List<Result> getResults(int userId) {
        return DBHelper.getResults(userId).stream().map(ModelConverter::convertResult)
                .collect(Collectors.toList());
    }
}
