package com.vedex.backend.common.dbmodel;

public class LessonEntity {
    private int lessonId;
    private String name;
    private String description;
    private String photo;
    private int form;
    private String subject;
    private int questionsNumber;
    private String packName;
    private int successRate;

    public LessonEntity(int lessonId, String name, String description, String photo, int form,
                        String subject, int questionsNumber, String packName, int successRate) {
        this.lessonId = lessonId;
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.form = form;
        this.subject = subject;
        this.questionsNumber = questionsNumber;
        this.packName = packName;
        this.successRate = successRate;
    }

    public int getLessonId() {
        return lessonId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPhoto() {
        return photo;
    }

    public int getForm() {
        return form;
    }

    public String getSubject() {
        return subject;
    }

    public int getQuestionsNumber() {
        return questionsNumber;
    }

    public String getPackName() {
        return packName;
    }

    public int getSuccessRate() {
        return successRate;
    }
}
