package com.vedex.backend.common.dbmodel.database;

import com.vedex.backend.common.dbmodel.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DBHelper {

    private static DBConnection db = DBConnection.getInstance();

    public static ProfileEntity getProfile(int userId) {
        ProfileEntity resultProfile = null;

        try {
            final String GET_PROFILE =
                    ResourceExtractor.getSql(ResourceExtractor.SQL_GET_PROFILE_USER_ID);

            Connection cn = db.getConnection();

            PreparedStatement getProfile = cn.prepareStatement(GET_PROFILE);

            getProfile.setInt(1, userId);

            ResultSet rs = getProfile.executeQuery();

            if(rs.next()) {
                resultProfile = DataExtractor.extractProfile(rs);
            }
            rs.close();
            getProfile.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultProfile;
    }

    public static void insertProfile(ProfileEntity profile) throws SQLException {
        final String INSERT_PROFILE =
                ResourceExtractor.getSql(ResourceExtractor.SQL_CREATE_PROFILE);

        Connection cn = db.getConnection();

        PreparedStatement insertProfile = cn.prepareStatement(INSERT_PROFILE);

        insertProfile.setString(1, profile.getName());
        insertProfile.setString(2, profile.getSurname());
        insertProfile.setString(3, profile.getLogin());
        insertProfile.setString(4,
                Optional.ofNullable(profile.getEmail()).orElse(""));
        insertProfile.setString(5,
                Optional.ofNullable(profile.getPhoto()).orElse(""));
        insertProfile.setString(6, profile.getPassword());

        insertProfile.executeUpdate();

        insertProfile.close();
    }

    public static void updateProfile(int userId, ProfileEntity profile) throws SQLException {
        final String UPDATE_PROFILE =
                ResourceExtractor.getSql(ResourceExtractor.SQL_UPDATE_PROFILE);

        Connection cn = db.getConnection();

        PreparedStatement updateProfile = cn.prepareStatement(UPDATE_PROFILE);

        updateProfile.setString(1, profile.getName());
        updateProfile.setString(2, profile.getSurname());
        updateProfile.setString(3,
                Optional.ofNullable(profile.getEmail()).orElse(""));
        updateProfile.setString(4,
                Optional.ofNullable(profile.getPhoto()).orElse(""));
        updateProfile.setString(5, profile.getPassword());
        updateProfile.setInt(6, userId);

        updateProfile.executeUpdate();

        updateProfile.close();
    }

    public static void updatePhoto(int userId, String photoUrl) {
        try {
            final String UPDATE_PROFILE_PHOTO = ResourceExtractor.getSql(
                    ResourceExtractor.SQL_UPDATE_USER_PHOTO);

            Connection cn = db.getConnection();

            PreparedStatement updatePhoto = cn.prepareStatement(UPDATE_PROFILE_PHOTO);

            updatePhoto.setString(1, photoUrl);
            updatePhoto.setInt(2, userId);

            updatePhoto.executeUpdate();

            updatePhoto.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getSubjects() {
        List<String> result = new ArrayList<>();
        try {
            final String GET_SUBJECTS =
                    ResourceExtractor.getSql(ResourceExtractor.SQL_GET_SUBJECTS);

            Connection cn = db.getConnection();

            PreparedStatement getSubjects = cn.prepareStatement(GET_SUBJECTS);

            ResultSet rs = getSubjects.executeQuery();

            while(rs.next()) {
                result.add(rs.getString("SUBJECT"));
            }

            rs.close();
            getSubjects.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static List<LessonEntity> getLessons(String subject) {
        List<LessonEntity> lessons = new ArrayList<>();
        try {
            final String GET_LESSONS = ResourceExtractor.getSql(ResourceExtractor.SQL_GET_LESSONS);

            Connection cn = db.getConnection();

            PreparedStatement getLessons = cn.prepareStatement(GET_LESSONS);

            getLessons.setString(1, subject);

            ResultSet rs = getLessons.executeQuery();

            while(rs.next()) {
                lessons.add(DataExtractor.extractLesson(rs));
            }

            rs.close();
            getLessons.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lessons;
    }

    public static void addResult(int userId, ResultEntity resultEntity) throws SQLException {
        final String ADD_RESULT = ResourceExtractor.getSql(ResourceExtractor.SQL_CREATE_RESULT);

        Connection cn = db.getConnection();

        PreparedStatement insertResult = cn.prepareStatement(ADD_RESULT);

        insertResult.setInt(1, resultEntity.getLesson().getLessonId());
        insertResult.setInt(2, userId);
        insertResult.setLong(3, resultEntity.getPassageTime());
        insertResult.setShort(4, resultEntity.getRightPercent());

        insertResult.executeUpdate();
        insertResult.close();
    }

    public static List<ResultEntity> getResults(int userId) {
        List<ResultEntity> results = new ArrayList<>();

        try {
            final String GET_RESULTS = ResourceExtractor.getSql(ResourceExtractor.SQL_GET_USER_RESULTS);

            Connection cn = db.getConnection();

            PreparedStatement getResults = cn.prepareStatement(GET_RESULTS);

            getResults.setInt(1, userId);

            ResultSet rs = getResults.executeQuery();

            while(rs.next()) {
                results.add(DataExtractor.extractResult(rs));
            }

            rs.close();
            getResults.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return results;
    }

    public static void createGroup(int userId, GroupEntity groupEntity) {
    }

    public static List<InvitationEntity> getInvitations(int userId) {
        List<InvitationEntity> invitations = new ArrayList<>();

        try {
            final String GET_INVITATIONS =
                    ResourceExtractor.getSql(ResourceExtractor.SQL_GET_INVITATIONS);

            Connection cn = db.getConnection();

            PreparedStatement getProfile = cn.prepareStatement(GET_INVITATIONS);

            getProfile.setInt(1, userId);

            ResultSet rs = getProfile.executeQuery();

            while(rs.next()) {
                invitations.add(DataExtractor.extractInvitation(rs));
            }
            rs.close();
            getProfile.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return invitations;
    }

    public static List<GroupMemberEntity> getGroupMembers(int userId, int groupId) {
        return new ArrayList<>();// check if user is a member of the group
    }

    public static List<GroupMembershipEntity> getGroups(int userId) {
        List<GroupMembershipEntity> groups = new ArrayList<>();

        try {
            final String GET_USER_GROUPS =
                    ResourceExtractor.getSql(ResourceExtractor.SQL_GET_USER_GROUPS);

            Connection cn = db.getConnection();

            PreparedStatement getUserGroups = cn.prepareStatement(GET_USER_GROUPS);

            getUserGroups.setInt(1, userId);

            ResultSet rs = getUserGroups.executeQuery();

            while(rs.next()) {
                groups.add(DataExtractor.extractGroupMembership(rs));
            }

            rs.close();
            getUserGroups.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return groups;
    }

    public static void createInvitation(int userId, InvitationEntity convertInvitation) {
    }

    public static void respondInvitation(int userId, int invitationId, boolean accepted) {

    }

    public static void editGroup(int userId, GroupEntity convertGroup) {

    }

    public static void editMemberAccess(
            int editorId, GroupMembershipEntity convertGroupMembership) {

    }

    public static void deleteGroup(int userId, int groupId) {

    }

    public static List<ProfileEntity> findProfiles(ProfileEntity matchProfileData) {
        return new ArrayList<>();
    }

    public static ProfileEntity getProfile(String login, String password) {
        ProfileEntity resultProfile = null;

        try {
            final String GET_PROFILE =
                    ResourceExtractor.getSql(ResourceExtractor.SQL_GET_PROFILE_LOGIN_PASSWORD);

            Connection cn = db.getConnection();

            PreparedStatement getProfile = cn.prepareStatement(GET_PROFILE);

            getProfile.setString(1, login);
            getProfile.setString(2, password);

            ResultSet rs = getProfile.executeQuery();

            if(rs.next()) {
                resultProfile = DataExtractor.extractProfile(rs);
            }
            rs.close();
            getProfile.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultProfile;
    }

    public static ProfileEntity getProfile(String login) {
        ProfileEntity resultProfile = null;

        try {
            final String GET_PROFILE =
                    ResourceExtractor.getSql(ResourceExtractor.SQL_GET_PROFILE_LOGIN);

            Connection cn = db.getConnection();

            PreparedStatement getProfile = cn.prepareStatement(GET_PROFILE);

            getProfile.setString(1, login);

            ResultSet rs = getProfile.executeQuery();

            if(rs.next()) {
                resultProfile = DataExtractor.extractProfile(rs);
            }
            rs.close();
            getProfile.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultProfile;
    }

    private static class DataExtractor {
        static ProfileEntity extractProfile(ResultSet rs) throws SQLException {
            return new ProfileEntity(rs.getInt("USERS.USER_ID"),
                    rs.getString("USERS.NAME"),
                    rs.getString("USERS.SURNAME"),
                    rs.getString("USERS.EMAIL"),
                    rs.getString("USERS.PHOTO"),
                    rs.getString("USERS.LOGIN"),
                    rs.getString("USERS.PASSWORD"));
        }

        static LessonEntity extractLesson(ResultSet rs) throws SQLException {
            return new LessonEntity(rs.getInt("LESSONS.LESSON_ID"),
                    rs.getString("LESSONS.NAME"),
                    rs.getString("LESSONS.DESCRIPTION"),
                    rs.getString("LESSONS.PHOTO"),
                    rs.getInt("LESSONS.FORM"),
                    rs.getString("LESSONS.SUBJECT"),
                    rs.getInt("LESSONS.QUESTIONS_NUMBER"),
                    rs.getString("LESSONS.PACKNAME"),
                    rs.getInt("SUCCESS_RATE"));
        }

        static InvitationEntity extractInvitation(ResultSet rs) throws SQLException {
            return new InvitationEntity(extractProfile(rs),
                    rs.getString("INVITATIONS.MESSAGE"),
                    rs.getDate("INVITATIONS.SENDING_DATE"),
                    extractGroup(rs));
        }

        static GroupEntity extractGroup(ResultSet rs) throws SQLException {
            return new GroupEntity(rs.getInt("GROUPS.GROUP_ID"),
                    rs.getString("GROUPS.NAME"),
                    rs.getString("GROUPS.DESCRIPTION"),
                    rs.getDate("GROUPS.CREATION_DATE"),
                    rs.getInt("GROUP_MEMBERS_COUNT"));
        }

        static GroupMembershipEntity extractGroupMembership(ResultSet rs) throws SQLException {
            return new GroupMembershipEntity(extractGroup(rs), extractGroupMember(rs));
        }

        static GroupMemberEntity extractGroupMember(ResultSet rs) throws SQLException {
            return new GroupMemberEntity(extractProfile(rs), rs.getString("ROLES.NAME"),
                    rs.getInt("ROLES.ACCESS_LEVEL"));
        }

        static ResultEntity extractResult(ResultSet rs) throws SQLException {
            return new ResultEntity(extractProfile(rs), extractLesson(rs), rs.getInt(
                    "PASSAGE_TIME"), rs.getShort("RIGHT_PERCENT"),
                    rs.getDate("RECORDING_DATE"));
        }
    }
}
