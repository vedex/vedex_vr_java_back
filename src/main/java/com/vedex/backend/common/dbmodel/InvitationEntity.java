package com.vedex.backend.common.dbmodel;

import java.util.Date;

public class InvitationEntity {
    private ProfileEntity sender;
    private String message;
    private Date sendingDate;
    private GroupEntity group;

    public InvitationEntity(ProfileEntity sender, String message, Date sendingDate, GroupEntity group) {
        this.sender = sender;
        this.message = message;
        this.sendingDate = sendingDate;
        this.group = group;
    }

    public ProfileEntity getSender() {
        return sender;
    }

    public String getMessage() {
        return message;
    }

    public Date getSendingDate() {
        return sendingDate;
    }

    public GroupEntity getGroup() {
        return group;
    }
}
