package com.vedex.backend.common.dbmodel;

import java.util.Date;

public class GroupEntity {
    private int id;
    private String name;
    private String description;
    private Date creationDate;
    private int membersNumber;

    public GroupEntity(int id, String name, String description, Date creationDate, int membersNumber) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
        this.membersNumber = membersNumber;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public int getMembersNumber() {
        return membersNumber;
    }
}
