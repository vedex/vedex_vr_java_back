package com.vedex.backend.common.dbmodel;

public class GroupMembershipEntity {
    private GroupEntity group;
    private GroupMemberEntity groupMember;

    public GroupMembershipEntity(GroupEntity group, GroupMemberEntity groupMember) {
        this.group = group;
        this.groupMember = groupMember;
    }

    public GroupEntity getGroup() {
        return group;
    }

    public GroupMemberEntity getGroupMember() {
        return groupMember;
    }
}
