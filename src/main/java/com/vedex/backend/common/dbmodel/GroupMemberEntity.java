package com.vedex.backend.common.dbmodel;

public class GroupMemberEntity {
    private ProfileEntity profile;
    private String role;
    private int accessLevel;

    public GroupMemberEntity(ProfileEntity profile, String role, int accessLevel) {
        this.profile = profile;
        this.role = role;
        this.accessLevel = accessLevel;
    }

    public ProfileEntity getProfile() {
        return profile;
    }

    public String getRole() {
        return role;
    }

    public int getAccessLevel() {
        return accessLevel;
    }
}
