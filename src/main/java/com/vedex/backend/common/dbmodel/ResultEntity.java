package com.vedex.backend.common.dbmodel;

import java.util.Date;

public class ResultEntity {
    private ProfileEntity profile;
    private LessonEntity lesson;
    private long passageTime;
    private short rightPercent;
    private Date recordingDate;

    public ResultEntity(ProfileEntity profile, LessonEntity lesson, long passageTime, short rightPercent, Date recordingDate) {
        this.profile = profile;
        this.lesson = lesson;
        this.passageTime = passageTime;
        this.rightPercent = rightPercent;
        this.recordingDate = recordingDate;
    }

    public ProfileEntity getProfile() {
        return profile;
    }

    public LessonEntity getLesson() {
        return lesson;
    }

    public long getPassageTime() {
        return passageTime;
    }

    public short getRightPercent() {
        return rightPercent;
    }

    public Date getRecordingDate() {
        return recordingDate;
    }
}
