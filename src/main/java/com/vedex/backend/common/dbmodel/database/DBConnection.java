package com.vedex.backend.common.dbmodel.database;

import com.microsoft.sqlserver.jdbc.SQLServerDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class to establish connectivity between server and datatransfer.
 * This is the final layer before datatransfer straight access.
 * Uses {@link Connection Connection} for connecting and  sending requests.
 */
class DBConnection {

    private static DBConnection instance;

    private static final String URL = "jdbc:sqlserver://vedex.database.windows.net:1433;" +
            "database=VedexDB;" +
            "encrypt=true;" +
            "trustServerCertificate=true;" +
            "hostNameInCertificate=*.database.windows.net;" +
            "loginTimeout=30;" +
            "useUnicode=true&" +
            "characterEncoding=UTF-8&" +
            "autoReconnect=true&" +
            "serverTimezone=UTC";

    private static final String USERNAME = "vedexadmin"; // vedexadmin@vedex ?
    private static final String PASSWORD = "LK8kmH7g";

    private Connection conn;

    private DBConnection(){
        try{
            SQLServerDriver driver = new SQLServerDriver();
            DriverManager.registerDriver(driver);
            conn = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            conn.setAutoCommit(true);
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    Connection getConnection(){
        try {
            if(!conn.isValid(0)) {
                conn.close();
                conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                conn.setAutoCommit(true);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    static DBConnection getInstance() {
        if(instance == null){
            instance = new DBConnection();
        }
        return instance;
    }
}
