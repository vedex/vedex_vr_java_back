package com.vedex.backend.common.dbmodel;

public class ProfileEntity {

    private int userId;
    private String name;
    private String surname;
    private String email;
    private String photo;
    private String login;
    private String password;

    public ProfileEntity(int userId, String name, String surname, String email,
                         String photo, String login, String password) {
        this.userId = userId;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.photo = photo;
        this.login = login;
        this.password = password;
    }

    public int getUserId() {
        return userId;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoto() {
        return photo;
    }
}
