package com.vedex.backend.common.dbmodel.database;

import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;

class ResourceExtractor {
    private static ClassLoader classLoader = ResourceExtractor.class.getClassLoader();

    private static HashMap<String, String> alreadyLoaded = new HashMap<>();

    static final String SQL_CREATE_PROFILE = "create_profile.sql";
    static final String SQL_CREATE_RESULT = "create_result.sql";
    static final String SQL_GET_INVITATIONS = "get_invitations.sql";
    static final String SQL_GET_LESSONS = "get_lessons.sql";
    static final String SQL_GET_PROFILE_LOGIN = "get_profile_login.sql";
    static final String SQL_GET_PROFILE_LOGIN_PASSWORD = "get_profile_login_password.sql";
    static final String SQL_GET_PROFILE_USER_ID = "get_profile_user_id.sql";
    static final String SQL_GET_SUBJECTS = "get_subjects.sql";
    static final String SQL_GET_USER_GROUPS = "get_user_groups.sql";
    static final String SQL_GET_USER_RESULTS = "get_user_results.sql";
    static final String SQL_UPDATE_PROFILE = "update_profile.sql";
    static final String SQL_UPDATE_USER_PHOTO = "update_user_photo.sql";

    static String getSql(String key) {
        if(alreadyLoaded.containsKey(key))
            return alreadyLoaded.get(key);

        StringBuilder result = new StringBuilder();
        Scanner in = new Scanner(
                Objects.requireNonNull(classLoader.getResourceAsStream("sql/" + key)));

        String line;
        while(in.hasNextLine()) {
            line = in.nextLine();
            result.append(line).append(" ");
        }

        in.close();

        if(result.length() > 0)
            alreadyLoaded.put(key, result.toString());
        return result.toString();
    }
}

