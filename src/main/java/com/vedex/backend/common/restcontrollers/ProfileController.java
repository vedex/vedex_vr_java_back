package com.vedex.backend.common.restcontrollers;

import com.vedex.backend.common.configuration.parameters.SessionParams;
import com.vedex.backend.common.dto.Profile;
import com.vedex.backend.common.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.List;

@RestController
public class ProfileController {

    private ProfileService profileService;

    @Autowired
    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public Profile getProfile(HttpServletRequest request) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        return profileService.getProfile(userId);
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public void updateProfile(HttpServletRequest request,
                       @RequestBody Profile profile) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        profileService.updateProfile(userId, profile);
    }

    @RequestMapping(value = "/photo", method = RequestMethod.POST)
    public void updatePhoto(HttpServletRequest request,
                     @RequestParam("photo_url") String photoUrl) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        profileService.updatePhoto(userId, photoUrl);
    }

    @RequestMapping(value = "/find", method = RequestMethod.POST)
    public List<Profile> findProfiles(HttpServletRequest request,
                                      @RequestBody Profile matchProfileData) {
        return profileService.findProfiles(matchProfileData);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Profile login(HttpServletRequest request,
                         @RequestBody Profile profile) {
        Profile userProfile = profileService.login(profile.getLogin(), profile.getPassword());

        if(userProfile != null) {
            request.getSession().setAttribute(SessionParams.USER_ID, userProfile.getUserId());
        }

        return userProfile;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request) {
        request.getSession().removeAttribute(SessionParams.USER_ID);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public void register(HttpServletRequest request,
                         @RequestBody Profile profile) throws SQLException {
        profileService.createProfile(profile);
    }
}
