package com.vedex.backend.common.restcontrollers;

import com.vedex.backend.common.dto.Lesson;
import com.vedex.backend.common.services.LessonsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class LessonsController {

    private LessonsService lessonsService;

    @Autowired
    public LessonsController(LessonsService lessonsService) {
        this.lessonsService = lessonsService;
    }

    @RequestMapping(value = "/subjects", method = RequestMethod.GET)
    List<String> getSubjects(HttpServletRequest request) {
        return lessonsService.getSubjects();
    }

    @RequestMapping(value = "/lessons", method = RequestMethod.GET)
    List<Lesson> getLessons(@RequestParam("subject") String subject) {
        return lessonsService.getLessons(subject);
    }
}
