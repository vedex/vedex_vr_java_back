package com.vedex.backend.common.restcontrollers;

import com.vedex.backend.common.configuration.parameters.SessionParams;
import com.vedex.backend.common.dto.*;
import com.vedex.backend.common.services.GroupsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class GroupsController {

    private GroupsService groupsService;

    @Autowired
    public GroupsController(GroupsService groupsService) {
        this.groupsService = groupsService;
    }

    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    List<GroupMembership> getGroups(HttpServletRequest request) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        return groupsService.getGroups(userId);
    }

    @RequestMapping(value = "/groups", method = RequestMethod.POST)
    void createGroup(HttpServletRequest request,
                     @RequestBody Group group) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        groupsService.createGroup(userId, group);
    }

    @RequestMapping(value = "/groups", method = RequestMethod.DELETE)
    void deleteGroup(HttpServletRequest request,
                     @RequestParam("group_id") int groupId) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        groupsService.deleteGroup(userId, groupId);
    }

    @RequestMapping(value = "/editGroup", method = RequestMethod.POST)
    void editGroup(HttpServletRequest request,
                   @RequestBody Group newState) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        groupsService.editGroup(userId, newState);
    }

    @RequestMapping(value = "/invitations", method = RequestMethod.GET)
    List<Invitation> getInvitations(HttpServletRequest request) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        return groupsService.getInvitations(userId);
    }

    @RequestMapping(value = "/invitations", method = RequestMethod.PUT)
    void createInvitation(HttpServletRequest request,
                          @RequestBody Invitation invitation) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        groupsService.createInvitation(userId, invitation);
    }

    @RequestMapping(value = "/invitations", method = RequestMethod.POST)
    void respondInvitation(HttpServletRequest request,
                          @RequestBody InvitationResponse response) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        groupsService.respondInvitation(userId, response);
    }

    @RequestMapping(value = "/members", method = RequestMethod.GET)
    List<GroupMember> getMembers(HttpServletRequest request,
                                 @RequestParam("group_id") int groupId) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        return groupsService.getMembers(userId, groupId);
    }

    @RequestMapping(value = "/members", method = RequestMethod.POST)
    void editMemberAccess(HttpServletRequest request,
                          @RequestBody GroupMembership newRoleDescription) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        groupsService.editMemberAccess(userId, newRoleDescription);
    }
}
