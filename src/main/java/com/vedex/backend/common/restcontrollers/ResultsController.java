package com.vedex.backend.common.restcontrollers;

import com.vedex.backend.common.configuration.parameters.SessionParams;
import com.vedex.backend.common.dto.Result;
import com.vedex.backend.common.services.ResultsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class ResultsController {

    private ResultsService resultsService;

    @Autowired
    public ResultsController(ResultsService resultsService) {
        this.resultsService = resultsService;
    }

    @RequestMapping(value = "/results", method = RequestMethod.POST)
    void addResult(HttpServletRequest request,
                      @RequestBody Result result) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        resultsService.addResult(userId, result);
    }

    @RequestMapping(value = "/results", method = RequestMethod.GET)
    List<Result> getResults(HttpServletRequest request) {
        int userId = (int) request.getSession().getAttribute(SessionParams.USER_ID);
        return resultsService.getResults(userId);
    }
}
