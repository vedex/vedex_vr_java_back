package com.vedex.backend.common.utils;

import com.vedex.backend.common.dbmodel.*;
import com.vedex.backend.common.dto.*;

public class ModelConverter {
    public static Profile convertProfile(ProfileEntity profile) {
        Profile convertedProfile = new Profile();
        convertedProfile.setUserId(profile.getUserId());
        convertedProfile.setLogin(profile.getLogin());
        convertedProfile.setPassword(profile.getPassword());
        convertedProfile.setName(profile.getName());
        convertedProfile.setSurname(profile.getSurname());
        convertedProfile.setEmail(profile.getEmail());
        convertedProfile.setPhoto(profile.getPhoto());

        return convertedProfile;
    }

    public static ProfileEntity convertProfile(Profile profile) {
        return new ProfileEntity(profile.getUserId(), profile.getName(), profile.getSurname(),
                profile.getEmail(), profile.getPhoto(), profile.getLogin(), profile.getPassword());
    }

    public static Lesson convertLesson(LessonEntity lesson) {
        Lesson convertedLesson = new Lesson();

        convertedLesson.setId(lesson.getLessonId());
        convertedLesson.setName(lesson.getName());
        convertedLesson.setDescription(lesson.getDescription());
        convertedLesson.setForm(lesson.getForm());
        convertedLesson.setPackageName(lesson.getPackName());
        convertedLesson.setPhotoUrl(lesson.getPhoto());
        convertedLesson.setQuestionsNumber(lesson.getQuestionsNumber());
        convertedLesson.setSubject(lesson.getSubject());
        convertedLesson.setSuccessRate(lesson.getSuccessRate());

        return convertedLesson;
    }

    public static LessonEntity convertLesson(Lesson lesson) {
        return new LessonEntity(lesson.getId(), lesson.getName(), lesson.getDescription(),
                lesson.getPhotoUrl(), lesson.getForm(), lesson.getSubject(),
                lesson.getQuestionsNumber(), lesson.getPackageName(), lesson.getSuccessRate());
    }

    public static ResultEntity convertResult(Result result) {
        return new ResultEntity(convertProfile(result.getUser()),
                convertLesson(result.getLesson()), result.getPassageTime(),
                result.getRightPercent(), result.getRecordingDate());
    }

    public static Result convertResult(ResultEntity result) {
        Result convertedResult = new Result();

        convertedResult.setLesson(convertLesson(result.getLesson()));
        convertedResult.setPassageTime(result.getPassageTime());
        convertedResult.setRecordingDate(result.getRecordingDate());
        convertedResult.setRightPercent(result.getRightPercent());
        convertedResult.setUser(convertProfile(result.getProfile()));

        return convertedResult;
    }

    public static GroupEntity convertGroup(Group group) {
        return new GroupEntity(group.getGroupId(), group.getName(), group.getDescription(),
                group.getCreationDate(), group.getMembersNumber());
    }

    public static Group convertGroup(GroupEntity group) {
        Group convertedGroup = new Group();

        convertedGroup.setCreationDate(group.getCreationDate());
        convertedGroup.setDescription(group.getDescription());
        convertedGroup.setGroupId(group.getId());
        convertedGroup.setMembersNumber(group.getMembersNumber());
        convertedGroup.setName(group.getName());

        return convertedGroup;
    }

    public static InvitationEntity convertInvitation(Invitation invitation) {
        return new InvitationEntity(convertProfile(invitation.getSender()),
                invitation.getMessage(), invitation.getSendingDate(),
                convertGroup(invitation.getGroup()));
    }

    public static Invitation convertInvitation(InvitationEntity invitation) {
        Invitation convertedInvitation = new Invitation();

        convertedInvitation.setGroup(convertGroup(invitation.getGroup()));
        convertedInvitation.setMessage(invitation.getMessage());
        convertedInvitation.setSender(convertProfile(invitation.getSender()));
        convertedInvitation.setSendingDate(invitation.getSendingDate());

        return convertedInvitation;
    }

    public static GroupMembership convertGroupMembership(
            GroupMembershipEntity groupMembershipEntity) {
        GroupMembership convertedGroupMembership = new GroupMembership();

        convertedGroupMembership.setGroup(convertGroup(groupMembershipEntity.getGroup()));
        convertedGroupMembership.setGroupMember(convertGroupMember(groupMembershipEntity
                .getGroupMember()));

        return convertedGroupMembership;
    }

    public static GroupMembershipEntity convertGroupMembership(GroupMembership groupMembership) {
        return new GroupMembershipEntity(convertGroup(groupMembership.getGroup()),
                convertGroupMember(groupMembership.getGroupMember()));
    }

    public static GroupMember convertGroupMember(GroupMemberEntity groupMemberEntity) {
        GroupMember convertedGroupMember = new GroupMember();

        convertedGroupMember.setAccessLevel(groupMemberEntity.getAccessLevel());
        convertedGroupMember.setProfile(convertProfile(groupMemberEntity.getProfile()));
        convertedGroupMember.setRole(groupMemberEntity.getRole());

        return convertedGroupMember;
    }

    public static GroupMemberEntity convertGroupMember(GroupMember groupMember) {
        return new GroupMemberEntity(convertProfile(groupMember.getProfile()),
                groupMember.getRole(), groupMember.getAccessLevel());
    }
}
