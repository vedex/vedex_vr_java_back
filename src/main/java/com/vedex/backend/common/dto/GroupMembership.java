package com.vedex.backend.common.dto;

public class GroupMembership {

    private Group group;
    private GroupMember groupMember;

    public GroupMembership() {
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public GroupMember getGroupMember() {
        return groupMember;
    }

    public void setGroupMember(GroupMember groupMember) {
        this.groupMember = groupMember;
    }
}
