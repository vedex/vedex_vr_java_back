package com.vedex.backend.common.dto;

public class InvitationResponse {

    private int invitationId;
    private boolean isAccepted;

    public InvitationResponse() {
    }

    public int getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(int invitationId) {
        this.invitationId = invitationId;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }
}
