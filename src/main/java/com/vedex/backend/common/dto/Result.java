package com.vedex.backend.common.dto;

import java.util.Date;

public class Result {

    private Profile user;
    private Lesson lesson;
    private long passageTime;
    private short rightPercent;
    private Date recordingDate;

    public Result() {
    }

    public Profile getUser() {
        return user;
    }

    public void setUser(Profile user) {
        this.user = user;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public long getPassageTime() {
        return passageTime;
    }

    public void setPassageTime(long passageTime) {
        this.passageTime = passageTime;
    }

    public short getRightPercent() {
        return rightPercent;
    }

    public void setRightPercent(short rightPercent) {
        this.rightPercent = rightPercent;
    }

    public Date getRecordingDate() {
        return recordingDate;
    }

    public void setRecordingDate(Date recordingDate) {
        this.recordingDate = recordingDate;
    }
}
