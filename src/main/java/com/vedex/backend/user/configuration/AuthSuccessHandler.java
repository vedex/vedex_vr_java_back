package com.vedex.backend.user.configuration;

import com.vedex.backend.common.configuration.parameters.SessionParams;
import com.vedex.backend.common.dto.Profile;
import com.vedex.backend.common.services.ProfileService;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;

public class AuthSuccessHandler implements AuthenticationSuccessHandler {

    private static AuthSuccessHandler instance;

    private AuthSuccessHandler() {

    }

    public static AuthSuccessHandler getInstance() {
        if(instance == null)
            instance = new AuthSuccessHandler();
        return instance;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException {
        String login =
                ((LinkedHashMap<String, String>)((OAuth2Authentication) authentication).getUserAuthentication().getDetails()).get("login");

        Profile profile = new ProfileService().getProfile(login);

        httpServletRequest.getSession().setAttribute(SessionParams.USER_ID, profile.getUserId());
    }
}
