select
    LESSONS.LESSON_ID,
    NAME,
    DESCRIPTION,
    FORM,
    PHOTO,
    QUESTIONS_NUMBER,
    SUCCESS_RATE,
    PACKNAME
from
    LESSONS
left join
    (select
        LESSON_ID,
        SUM(RIGHT_PERCENT) / COUNT(*) as SUCCESS_RATE
    from
        RESULTS
    group by LESSON_ID) t
on t.lesson_id=lessons.lesson_id
where SUBJECT=?;
