select
    GROUPS.*,
    USERS.*,
    ROLES.NAME,
    ROLES.ACCESS_LEVEL
from
        (select
            GROUPS.*,
            GROUP_MEMBERS_COUNT
        from
                GROUPS
            inner join
                (select
                    GROUP_ID,
                    COUNT(*) as GROUP_MEMBERS_COUNT
                from
                    GROUP_MEMBERS
                group by GROUP_ID) as GROUP_MEMBERS on GROUP_MEMBERS.GROUP_ID=GROUPS.GROUP_ID) as GROUPS
    inner join GROUP_MEMBERS on GROUP_MEMBERS.GROUP_ID=GROUPS.GROUP_ID
    inner join USERS on USERS.USER_ID=GROUP_MEMBERS.USER_ID
    inner join ROLES on ROLES.ROLE_ID=GROUP_MEMBERS.ROLE_ID
where USERS.USER_ID=?;