select
    *
from
        INVITATIONS
    inner join
        USERS on USERS.USER_ID=INVITATIONS.SENDER_ID
    inner join
        (select
            GROUPS.*,
            GROUP_MEMBERS_COUNT
         from
                GROUPS
            inner join
                (select
                    GROUP_ID,
                    COUNT(*) as GROUP_MEMBERS_COUNT
                from
                    GROUP_MEMBERS
                group by GROUP_ID) as GROUP_MEMBERS on GROUP_MEMBERS.GROUP_ID=GROUPS.GROUP_ID)
        as GROUPS on GROUPS.GROUP_ID=INVITATIONS.GROUP_ID
where INVITATIONS.RECEIVER_ID=?;